using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSystem : MonoBehaviour
{
    public int maxHP = 600;
    public float currentHealth;
    public SpriteRenderer spriteBoss;
    [SerializeField] public GameObject explosion;
    [SerializeField] public BoxCollider2D bc;
    public float bulletSpeed = 10;
    public float shootRate = 1f;
    public bool visible = false;
    [SerializeField] public bool die = false;
    [SerializeField] public float speed;

    void Start()
    {
        spriteBoss.GetComponent<SpriteRenderer>().enabled = false;
        currentHealth = maxHP; 
        explosion.SetActive(false);

    }

    void Update()
    {
        if(gameObject.transform.position.x < 210)
        {
            gameObject.transform.Translate(new Vector2(speed, 0) * Time.deltaTime, Space.World);           
        }
        else
        {
            visible = true;
            spriteBoss.GetComponent<SpriteRenderer>().enabled = true;
        }
        

    }


    void TakeDamage(float damage)
    {
        currentHealth = currentHealth - damage;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Bullet" || collision.tag == "Player")
        {
            TakeDamage(20);

            StartCoroutine(FlashRed());
        }
        if(collision.tag == "Bullet" && currentHealth == 0)
        {
            StartCoroutine(ExplosionDie());

            die = true;
        }
    }

    private IEnumerator FlashRed()
    {
        spriteBoss.color = Color.red;
        yield return new WaitForSeconds(0.2f);
        spriteBoss.color = Color.white;
    }

    private IEnumerator ExplosionDie()
    {
        explosion.SetActive(true);
        bc.enabled = false;

        WaitForSeconds wait = new WaitForSeconds(0.7f);
        yield return wait;

        Destroy(gameObject);
    }

}
