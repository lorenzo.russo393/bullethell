using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Unity.VisualScripting;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] public float cameraSpeed;

    public bool stopForward = false;

    private void Start()
    {
        
    }

    private void Update()
    {
        if(gameObject.transform.position.x < 205)
        {
            CameraForward();
        }
        else
        {
            stopForward = true;    
        }
    }

    void CameraForward()
    {
        gameObject.transform.Translate(new Vector2(cameraSpeed, 0) * Time.deltaTime, Space.World);
    }

    void CameraStop()
    {

    }

}
