using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFast : MonoBehaviour
{
    [SerializeField] public float speed;

    public GameObject explosion;
    public BoxCollider2D bo;

    public float radius;
    [SerializeField] LayerMask playerMask;

    void Start()
    {
        explosion.SetActive(false);
    }

    private void Update()
    {   
        Movement();    
    }

    private void Movement()
    {
        gameObject.transform.Translate(new Vector2(-speed, 0) * Time.deltaTime, Space.World);

        /*  transform.position = Vector3.MoveTowards(transform.position, patrolPoints[index].transform.position, speed);

          if (Vector3.Distance(transform.position, patrolPoints[index].transform.position) < 1)
          {
              index++;
          }

          if (index == 5)
          {
              index = 0;


          }*/

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bullet" || collision.tag == "Player")
        {
            StartCoroutine(ExplosionDie());
        }
        if(collision.tag == "WallLeft")
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator ExplosionDie()
    {
        explosion.SetActive(true);
        bo.enabled = false;

        WaitForSeconds wait = new WaitForSeconds(0.5f);
        yield return wait;

        Destroy(gameObject);
    }
}
