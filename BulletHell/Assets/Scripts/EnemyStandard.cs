using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStandard : MonoBehaviour
{
    [SerializeField] public float speed;

    public GameObject explosion;
    public BoxCollider2D boG;
    public BoxCollider2D boS;

    // Start is called before the first frame update
    void Start()
    {
        explosion.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Translate(new Vector2(-speed, 0) * Time.deltaTime, Space.World);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bullet" || collision.tag == "Player")
        {
            StartCoroutine(ExplosionDie());
        }

        if (collision.tag == "WallLeft")
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator ExplosionDie()
    {
        explosion.SetActive(true);
        boG.enabled = false;
        boS.enabled = false;

        WaitForSeconds wait = new WaitForSeconds(0.5f);
        yield return wait;

        Destroy(gameObject);
    }
}
