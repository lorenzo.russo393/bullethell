using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class EnemyTank : MonoBehaviour
{
    [SerializeField] public float speed;
    [SerializeField] public GameObject explosion;
    [SerializeField] public BoxCollider2D bc;
    [SerializeField] public SpriteRenderer sprite;

    private int maxHP = 40;
    public int currentHP;

    void Start()
    {
        explosion.SetActive(false);
        currentHP = maxHP;    
    }

    // Update is called once per frame
    void Update()
    {
        Movement();    
    }

    void Movement()
    {
        gameObject.transform.Translate(new Vector2(-speed, 0) * Time.deltaTime, Space.World);
    }

    public void TakeDamage(int damage)
    {
        currentHP = currentHP - damage;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bullet")
        {
            TakeDamage(20);
            StartCoroutine(FlashRed());
        }

        if (collision.tag == "Player")
        {
            TakeDamage(10);
            StartCoroutine(FlashRed());
        }

        if ((collision.tag == "Bullet" || collision.tag == "Player") && currentHP == 0)
        {
            StartCoroutine(ExplosionDie());
        }

        if (collision.tag == "WallLeft")
        {
            Destroy(gameObject);
        }
    }

    private IEnumerator ExplosionDie()
    {
        explosion.SetActive(true);
        bc.enabled = false;

        WaitForSeconds wait = new WaitForSeconds(0.5f);
        yield return wait;

        Destroy(gameObject);
    }

    private IEnumerator FlashRed()
    {
        sprite.color = Color.red;
        yield return new WaitForSeconds(0.2f);
        sprite.color = Color.white;
    }
}
