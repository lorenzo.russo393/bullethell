using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float movementSpeed;
    public float speed;
    private float dirX;
    private float dirY;

    public int maxHealth = 100;
    public float currentHealth;

    public HealtbarPlayer healthbarP;

    [SerializeField] public SpriteRenderer sprite;

    [SerializeField] public Rigidbody2D rb;

    Vector3 movement;

    [SerializeField] public GameObject boost;

    CameraMovement cameraMovement;

    [SerializeField] public bool isDie = false;
    public bool isMoving = true;

    [SerializeField] public GameObject explosion;
    [SerializeField] public BoxCollider2D bc;

    void Start()
    {
        explosion.SetActive(false);
        currentHealth = maxHealth;
        healthbarP.setMaxHealth(maxHealth);

        movementSpeed = 2f; 

        boost.SetActive(false);

        cameraMovement= FindObjectOfType<CameraMovement>();
    }


    // Update is called once per frame
    void Update()
    {
        if(isMoving)
        {
            Move();
            movement = new Vector3(Input.GetAxis("Horizontal") * speed, (Input.GetAxis("Vertical")) * speed, 0.0f);
            spawnBoost();           
        }
        else
        {
            rb.velocity = new Vector2(0, 0);
        }

        //dirX = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        //transform.position = new Vector2(transform.position.x + dirX, transform.position.y);

        //dirY = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        //transform.position = new Vector2(transform.position.x, transform.position.y + dirY);

        if (!cameraMovement.stopForward)
        {
            gameObject.transform.Translate(new Vector2(movementSpeed, 0) * Time.deltaTime, Space.World);
        }
    }
    void spawnBoost()
    {
        if(Input.GetKey(KeyCode.D))
        {
            boost.SetActive(true);
        }
        else
        {
            boost.SetActive(false);
        }

    }

    public void TakeDamage(float damage)
    {
        currentHealth = currentHealth - damage;

        healthbarP.SetHealt(currentHealth);
    }

   private void OnTriggerEnter2D(Collider2D collision)
   {
       if(collision.tag == "BulletEnemy")
       {
           TakeDamage(5);

            if (currentHealth > 0)
            {
                StartCoroutine(FlashRed());
            }
        }

        if (collision.tag == "FireBall")
        {
            TakeDamage(10);

            if (currentHealth > 0)
            {
                StartCoroutine(FlashRed());
            }
        }

        if (collision.tag == "Enemy")
        {
            TakeDamage(5);

            if(currentHealth> 0)
            {
                StartCoroutine(FlashRed());
            }
            
        }

        if ((collision.tag == "BulletEnemy" || collision.tag == "Enemy" || collision.tag == "FireBall") && currentHealth == 0)
       {
            isDie= true;
            StartCoroutine(ExplosionDie());
       }

    }

    private IEnumerator FlashRed()
    {
        sprite.color = Color.red;
        yield return new WaitForSeconds(0.2f);
        sprite.color = Color.white;
    }

    private IEnumerator ExplosionDie()
    {
        explosion.SetActive(true);

        WaitForSeconds wait = new WaitForSeconds(0.5f);
        yield return wait;

        Destroy(gameObject);
    }

    void Move()
    {
        rb.velocity = new Vector2(movement.x, movement.y);
    }
}
