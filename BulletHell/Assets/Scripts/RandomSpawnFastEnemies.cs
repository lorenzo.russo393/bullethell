using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnFastEnemies : MonoBehaviour
{
    [SerializeField] public GameObject enemyFastPrefab;

    private bool spawnFastEnemy;
    private float randomSpawnZone;
    private Vector2 spawnPosition;

    private void Start()
    {
        spawnFastEnemy = false;

        InvokeRepeating("SpawnFastEnemy", 0f, 0.8f);
    }

    private void Update()
    {
        if(gameObject.transform.position.x > 72)
        {
            spawnFastEnemy = true;
        }
        if (gameObject.transform.position.x > 130)
        {
            spawnFastEnemy = false;
        }
    }

    private void SpawnFastEnemy()
    {
            float app = randomSpawnZone;
            randomSpawnZone = Random.Range(4.5f, -2.7f);

            spawnPosition = new Vector2(gameObject.transform.transform.position.x, randomSpawnZone);

            if (spawnFastEnemy && randomSpawnZone != app)
            {

                Instantiate(enemyFastPrefab, spawnPosition, Quaternion.identity);
            }
    }
}
