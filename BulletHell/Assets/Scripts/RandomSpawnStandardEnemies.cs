using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class RandomSpawnStandardEnemies : MonoBehaviour
{
    [SerializeField] public GameObject enemyStandardPrefab;

    private float randomSpawnZone;
    private Vector2 spawnPosition;

    private bool spawnStandardEnemy;

    private void Start()
    {
        spawnStandardEnemy = true;    
        InvokeRepeating("SpawnStandardEnemy", 0f, 1f);    
    }

    private void Update()
    {
        if (gameObject.transform.position.x >= 63)
        {
            spawnStandardEnemy = false;
        }
    }

    private void SpawnStandardEnemy()
    {
        if (gameObject.transform.position.x > 15)
        {
            if (spawnStandardEnemy)
            {
                float app = randomSpawnZone;
                randomSpawnZone = Random.Range(4.5f, -2.7f);

                spawnPosition = new Vector2(gameObject.transform.transform.position.x, randomSpawnZone);

                if (spawnStandardEnemy && randomSpawnZone != app)
                {
                    Instantiate(enemyStandardPrefab, spawnPosition, Quaternion.identity);
                }
            }
        }
    }
}
