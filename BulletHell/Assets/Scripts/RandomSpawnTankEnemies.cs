using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnTankEnemies : MonoBehaviour
{
    [SerializeField] public GameObject enemyTankPrefab;

    private bool spawnTankEnemy;
    private float randomSpawnZone;
    private Vector2 spawnPosition;

    void Start()
    {
        spawnTankEnemy = false;

        InvokeRepeating("SpawnTankEnemy", 0f, 1.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position.x > 142)
        {
            spawnTankEnemy = true;
        }
        if (gameObject.transform.position.x > 193)
        {
            spawnTankEnemy = false;
        }
    }

    void SpawnTankEnemy()
    {
        float app = randomSpawnZone;
        randomSpawnZone = Random.Range(4.5f, -2.7f);

        spawnPosition = new Vector2(gameObject.transform.transform.position.x, randomSpawnZone);

        if (spawnTankEnemy && randomSpawnZone != app)
        {

            Instantiate(enemyTankPrefab, spawnPosition, Quaternion.identity);
        }
    }
}
