using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBoss : MonoBehaviour
{
    [SerializeField] public GameObject bulletPrefab;
    public float bulletSpeed = 10;
    public float shootRate = 1f;
    [SerializeField] public Transform muzzle;
    private bool stop =false;

    private CameraMovement cm;
    void Start()
    {


        StartCoroutine(Shooting());
    }

    // Update is called once per frame
    void Update()
    {
        cm = FindObjectOfType<CameraMovement>();

        stop = cm.stopForward;
    }

    private IEnumerator Shooting()
    {

        WaitForSeconds wait = new WaitForSeconds(shootRate);

        while (true)
        {

            yield return wait;

            var bullet = Instantiate(bulletPrefab, muzzle.position, muzzle.rotation);
            bullet.GetComponent<Rigidbody2D>().velocity = muzzle.right * bulletSpeed;

        }
        
    }
}
