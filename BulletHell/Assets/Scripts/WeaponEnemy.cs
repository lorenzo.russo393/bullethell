using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponEnemy : MonoBehaviour
{
    [SerializeField] public Transform muzzle;
    [SerializeField] public GameObject bulletPrefab;
    public float bulletSpeed = 10;
    public float shootRate = 4f;

    void Start()
    {
        StartCoroutine(Shooting());       
    }

    private IEnumerator Shooting()
    {
        WaitForSeconds wait = new WaitForSeconds(shootRate);
        yield return wait;

        var bullet = Instantiate(bulletPrefab, muzzle.position, muzzle.rotation);
        bullet.GetComponent<Rigidbody2D>().velocity = muzzle.right * bulletSpeed;
    }
}
