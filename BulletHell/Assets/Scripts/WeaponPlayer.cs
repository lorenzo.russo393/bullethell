using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPlayer : MonoBehaviour
{
    [SerializeField] public Transform muzzle;
    [SerializeField] public GameObject bulletPrefab;
    public float bulletSpeed = 10;

    private void Start()
    {
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            var bullet = Instantiate(bulletPrefab, muzzle.position, muzzle.rotation);           
            bullet.GetComponent<Rigidbody2D>().velocity = muzzle.right * bulletSpeed;
        }
    }
}
