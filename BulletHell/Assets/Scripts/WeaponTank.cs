using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponTank : MonoBehaviour
{
    [SerializeField] public Transform muzzle;
    [SerializeField] public GameObject bulletPrefab;
    public float bulletSpeed = 10;
    public float shootRate = 4f;
    int bulletToShoot = 2;

    void Start()
    {
        StartCoroutine(Shooting());
    }

    private IEnumerator Shooting()
    {
        int index = 0;

        WaitForSeconds wait = new WaitForSeconds(shootRate);

        while (index < bulletToShoot)
        {
            yield return wait;

            var bullet = Instantiate(bulletPrefab, muzzle.position, muzzle.rotation);
            bullet.GetComponent<Rigidbody2D>().velocity = muzzle.right * bulletSpeed;

            index++;
        }
    }
}
