using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLose : MonoBehaviour
{
    public GameObject gameOver;
    public GameObject win;
    public GameObject player;
    private PlayerMovement pl;
    private BossSystem bs;

    // Start is called before the first frame update
    void Start()
    {
        pl = FindObjectOfType<PlayerMovement>();
        bs = FindObjectOfType<BossSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (pl.isDie)
        {
            GameOver();
        }

        if (bs.die)
        {
            Win();
        }

    }

    void GameOver()
    {
        gameOver.SetActive(true);
    }


    void Win()
    {
        win.SetActive(true);

        pl.isMoving = false;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
